# frozen_string_literal: true

require 'lib'
require 'common'
require 'specification'

class CipherDagBuilder
  attr_reader :states, :functions, :names, :transitions

  def initialize
    @state_ids = {}
    @states = []
    @plaintext = []
    @key = []
    @ciphertext = []
    @functions = []
    @transitions = []
    @tmp_var = 0
    @names = {}
    @blocks = {}
  end

  private def const(value)
    name = value.to_s
    unless @state_ids.has_key?(name)
      @state_ids[name] = AttachedStateNode.new(self, Specification::StateNodeUID.new(uid: @state_ids.size))
      @states.push Specification::Constant.new(value: value)
    end
    @state_ids[name]
  end

  private def var(name, domain)
    unless @state_ids.has_key?(name)
      @state_ids[name] = AttachedStateNode.new(self, Specification::StateNodeUID.new(uid: @state_ids.size))
      @states.push Specification::Variable.new(domain: domain)
    end
    @state_ids[name]
  end

  public
  def tmp_var(domain)
    var = var("tmp_var_" + @tmp_var.to_s, domain)
    @tmp_var += 1
    var
  end

  def convert_int_to_constants(inputs)
    inputs.map do |i|
      if i.is_a?(Integer)
        const(i)
      elsif i.is_a?(AttachedStateNode)
        i
      else
        raise "invalid input #{i}"
      end
    end
  end

  def declare_sbox_function(domain, co_domain, lookup_table)
    function_id = Specification::FunctionNodeUID.new(uid: @functions.size)
    @functions.push Specification::FunctionNode.new(domain: domain, co_domain: co_domain, function: Specification::SBox.new(lookup_table: lookup_table))
    AttachedFunctionNode.new(self, function_id)
  end

  def declare_linear_function(domain, co_domain, f)
    function_id = Specification::FunctionNodeUID.new(uid: @functions.size)
    @functions.push Specification::FunctionNode.new(domain: domain, co_domain: co_domain, function: Specification::Linear.new(linear: f))
    AttachedFunctionNode.new(self, function_id)
  end

  def build
    Lib::SpecificationGraph.new(
      graph: Specification::SpecificationGraph.new(
        version: Version.new(major: 0, minor: 10, patch: 2),
        states: @states,
        functions: @functions,
        transitions: @transitions,
        plaintext: @plaintext.map(&:state_node_uid),
        key: @key.map(&:state_node_uid),
        ciphertext: @ciphertext.map(&:state_node_uid),
        names: @names,
        blocks: @blocks
      )
    )
  end

  def plaintext(len, domain)
    @plaintext = named("__PLAINTEXT__", Array.new(len) { |i| var('plaintext_' + i.to_s, domain).alias("__P_#{i}__") })
    @plaintext[0..-1]
  end

  def key(len, domain)
    @key = named("__KEY__", Array.new(len) { |i| var('key_' + i.to_s, domain).alias("__K_#{i}__") })
    @key[0..-1]
  end

  def named(name, iter)
    elements = iter.flatten
    for i in 0..elements.length - 1
      elements[i].alias("#{name}__#{i}")
    end
    @blocks[name] = elements.map(&:state_node_uid)
    iter
  end

  def ciphertext(vars)
    @ciphertext = named("__CIPHERTEXT__", vars.flatten.map.with_index { |var, i| var.alias("__C_#{i}__") })
  end
end

class AttachedStateNode
  attr_reader :state_node_uid

  def initialize(dag_builder, state_node_uid)
    @builder = dag_builder
    @state_node_uid = state_node_uid
  end

  def state_node
    @builder.states[@state_node_uid.uid]
  end

  def alias(name)
    @builder.names[name] = @state_node_uid
    self
  end

  def to_s
    state_node.to_s
  end

end

class AttachedFunctionNode
  attr_reader :function_node_uid

  def initialize(dag_builder, function_node_uid)
    @builder = dag_builder
    @function_node_uid = function_node_uid
  end

  def function
    @builder.functions[@function_node_uid.uid]
  end

  def call(*inputs)
    unless inputs.size == function.domain.size
      raise "invalid number of inputs. Expected #{function.domain.size}, given #{inputs.size}"
    end
    co_domain = function.co_domain
    outputs = co_domain.map { |it| @builder.tmp_var(it) }
    # puts inputs
    @builder.transitions.push Specification::Transition.new(
      inputs: @builder.convert_int_to_constants(inputs).map(&:state_node_uid),
      function: @function_node_uid,
      outputs: outputs.map(&:state_node_uid)
    )
    if outputs.size == 1
      outputs[0]
    else
      outputs
    end
  end

  def to_s
    function.to_s
  end

end