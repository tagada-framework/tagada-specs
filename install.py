#!/usr/bin/python3

import argparse
import os

import yaml

import shell
from shell import Bin, extended_path
from utils import download_and_execute, set_executable, eprint


def install_rvm():
    eprint("Installing rvm")
    download_and_execute("https://get.rvm.io", "rvm.io")
    set_executable(os.path.expanduser("~/.rvm/bin/rvm"))


def install_ruby(dependencies):
    eprint(f"Installing ruby {dependencies['ruby']}")
    rvm.install(dependencies["ruby"])


def install_gems():
    bundle.config("set", "--local", "path", "vendor/bundle")
    bundle.install()


def generate_env(dependencies):
    env_sh = f"""
    #!/usr/bin/bash
    # Specs
    export SPECS="$TAGADA_HOME/tagada-specs"
    ## Add custom ruby to path
    export PATH="$HOME/.rvm/rubies/ruby-{dependencies['ruby']}/bin:$PATH"
    export BUNDLE_GEMFILE="$SPECS/Gemfile"
    export BUNDLE_PATH="$SPECS/."
    """
    with open("env.sh", "w") as env_sh_file:
        env_sh_file.write(env_sh.strip())


if __name__ == "__main__":
    parser = argparse.ArgumentParser("install.py")
    parser.add_argument("-v", "--verbose", help="enable verbosity", action="store_true")
    args = parser.parse_args()
    if args.verbose:
        shell.VERBOSITY = True

    with open("config.yml", "r") as f:
        config = yaml.safe_load(f)
    dependencies = config["dependencies"]
    # Install ruby
    install_rvm()
    set_executable("rvm")
    rvm = Bin(os.path.realpath("rvm"))
    rvm.install = rvm % "install"
    install_ruby(dependencies)
    # Install tagada required gems
    with extended_path(os.path.expanduser(f"~/.rvm/rubies/ruby-{dependencies['ruby']}/bin")):
        bundle = Bin("bundle")
        bundle.config = bundle % "config"
        bundle.install = bundle % "install"
        install_gems()
    # Make the cipher scripts executable
    for cipher in os.listdir("ciphers"):
        if os.path.isfile(cipher) and cipher.endswith(".rb"):
            set_executable(cipher)
    # Generate the env.sh file
    generate_env(dependencies)
