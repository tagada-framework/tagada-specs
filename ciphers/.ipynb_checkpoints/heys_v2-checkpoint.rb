#!/usr/bin/env ruby

require 'slop'
require 'common'
require 'specification'

require_relative '../builders/cipher_dag_builder'
require_relative '../builders/helpers'

def process_options(arg)
  begin
    opts = Slop.parse(arg) do |o|
      o.int '-r', '--round', 'number of rounds', default: 5
      o.on '--help', 'print the help' do
        puts o
        exit
      end
    end
  rescue Slop::Error => e
    warn e
    exit(2)
  end
  opts
end

HEYS_SBOX_LOOKUP_TABLE = [
  0xE, 0x4, 0xD, 0x1, 0x2, 0xF, 0xB, 0x8, 0x3, 0xA, 0x6, 0xC, 0x5, 0x9, 0x0, 0x7
]

NIBBLE = HalfOpenRange.new(min: 0, max: 16)
NIBBLE_X4 = HalfOpenRange.new(min: 0, max: 16 * 16 * 16 * 16)
class HeysConfig
  def initialize(options)
    # We retrieve the number of round
    @nr = options["round"] || 5
    # We create a graph builder
    @builder = CipherDagBuilder.new()
    # We define the cipher functions
    @xor2 = @builder.declare_linear_function([NIBBLE, NIBBLE], [NIBBLE], Specification::BitwiseXOR.new)
    @p = @builder.declare_linear_function([NIBBLE, NIBBLE, NIBBLE, NIBBLE], [NIBBLE_X4], Specification::Concat.new(to_word: [
      Specification::BitAndWord.new(bit_pos:3, word_pos: 0),
      Specification::BitAndWord.new(bit_pos:3, word_pos: 1),
      Specification::BitAndWord.new(bit_pos:3, word_pos: 2),
      Specification::BitAndWord.new(bit_pos:3, word_pos: 3),

      Specification::BitAndWord.new(bit_pos:2, word_pos: 0),
      Specification::BitAndWord.new(bit_pos:2, word_pos: 1),
      Specification::BitAndWord.new(bit_pos:2, word_pos: 2),
      Specification::BitAndWord.new(bit_pos:2, word_pos: 3),

      Specification::BitAndWord.new(bit_pos:1, word_pos: 0),
      Specification::BitAndWord.new(bit_pos:1, word_pos: 1),
      Specification::BitAndWord.new(bit_pos:1, word_pos: 2),
      Specification::BitAndWord.new(bit_pos:1, word_pos: 3),

      Specification::BitAndWord.new(bit_pos:0, word_pos: 0),
      Specification::BitAndWord.new(bit_pos:0, word_pos: 1),
      Specification::BitAndWord.new(bit_pos:0, word_pos: 2),
      Specification::BitAndWord.new(bit_pos:0, word_pos: 3),
    ]))
    @split = @builder.declare_linear_function([NIBBLE_X4], [NIBBLE, NIBBLE, NIBBLE, NIBBLE], Specification::Split.new(to_words: [
      [15,14,13,12], [11,10,9,8], [7,6,5,4], [3,2,1,0]
    ]
    ))
    @s = @builder.declare_sbox_function([NIBBLE], [NIBBLE], HEYS_SBOX_LOOKUP_TABLE)
  end

  private
  def encryption(plaintext, key, nr)
    k = keyschedule(key, nr)
    u = Array.new(4) { |j| @xor2.call(plaintext[j], k[0][j]) }
    @builder.named("u1", u)
    for i in 1...@nr - 1
      v = Array.new(4) { |j| @s.call(u[j]) }
      @builder.named("v#{1}", v)
      w_full = @p.call(v[0], v[1], v[2], v[3])
      w = @split.call(w_full)
      @builder.named("w#{1}", w)
      u = Array.new(4) { |j| @xor2.call(w[j], k[i][j]) }
      @builder.named("u#{i + 1}", u)
    end
    v = Array.new(4) { |j| @s.call(u[j]) }
    @builder.named("v#{nr - 1}", v)
    y = Array.new(4) { |j| @xor2.call(v[j], k[nr - 1][j]) }
    @builder.named("y", y)
    y
  end

  def keyschedule(key, nr)
    round_keys = Array.new(nr) { |i| [key[i], key[i + 1],key[i + 2], key[i + 3]] }
    round_keys.each_with_index { |round_key, index| @builder.named("K_#{index + 1}", round_key) }
    round_keys
  end

  public
  def build()
    plaintext = @builder.plaintext(4, NIBBLE)
    key = @builder.key(8, NIBBLE)
    @builder.ciphertext(encryption(plaintext, key, @nr))
    @builder.build
  end
end

# usage: heys.rb --round 5
def main
  options = process_options(ARGV)
  puts HeysConfig.new(options).build.to_json
end

main
