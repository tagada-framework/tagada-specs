#!/usr/bin/env ruby

require 'slop'
require 'common'
require 'specification'
require_relative '../builders/cipher_dag_builder'
require_relative '../builders/helpers'

def process_options(arg)
  begin
    opts = Slop.parse(arg) do |o|
      o.int '-v', '--version', 'version {64, 128}'
      o.int '-r', '--round', 'number of rounds {1,2,...}'
      o.int '-tk', '--tweak-key', 'Skinny inputs configuration {0,1,2,3}'
      o.on '--help', 'print the help' do
        puts o
        exit
      end
    end
  rescue Slop::Error => e
    warn e
    exit(2)
  end
  opts
end

BYTE = HalfOpenRange.new(min: 0, max: 256)
NIBBLE = HalfOpenRange.new(min: 0, max: 16)

SKINNY64_SBOX_LOOKUP_TABPLE = [
  12, 6, 9, 0, 1, 10, 2, 11, 3, 8, 5, 13, 4, 14, 7, 15
]

SKINNY128_SBOX_LOOKUP_TABPLE = [
  0x65, 0x4c, 0x6a, 0x42, 0x4b, 0x63, 0x43, 0x6b, 0x55, 0x75, 0x5a, 0x7a, 0x53, 0x73, 0x5b, 0x7b,
  0x35, 0x8c, 0x3a, 0x81, 0x89, 0x33, 0x80, 0x3b, 0x95, 0x25, 0x98, 0x2a, 0x90, 0x23, 0x99, 0x2b,
  0xe5, 0xcc, 0xe8, 0xc1, 0xc9, 0xe0, 0xc0, 0xe9, 0xd5, 0xf5, 0xd8, 0xf8, 0xd0, 0xf0, 0xd9, 0xf9,
  0xa5, 0x1c, 0xa8, 0x12, 0x1b, 0xa0, 0x13, 0xa9, 0x05, 0xb5, 0x0a, 0xb8, 0x03, 0xb0, 0x0b, 0xb9,
  0x32, 0x88, 0x3c, 0x85, 0x8d, 0x34, 0x84, 0x3d, 0x91, 0x22, 0x9c, 0x2c, 0x94, 0x24, 0x9d, 0x2d,
  0x62, 0x4a, 0x6c, 0x45, 0x4d, 0x64, 0x44, 0x6d, 0x52, 0x72, 0x5c, 0x7c, 0x54, 0x74, 0x5d, 0x7d,
  0xa1, 0x1a, 0xac, 0x15, 0x1d, 0xa4, 0x14, 0xad, 0x02, 0xb1, 0x0c, 0xbc, 0x04, 0xb4, 0x0d, 0xbd,
  0xe1, 0xc8, 0xec, 0xc5, 0xcd, 0xe4, 0xc4, 0xed, 0xd1, 0xf1, 0xdc, 0xfc, 0xd4, 0xf4, 0xdd, 0xfd,
  0x36, 0x8e, 0x38, 0x82, 0x8b, 0x30, 0x83, 0x39, 0x96, 0x26, 0x9a, 0x28, 0x93, 0x20, 0x9b, 0x29,
  0x66, 0x4e, 0x68, 0x41, 0x49, 0x60, 0x40, 0x69, 0x56, 0x76, 0x58, 0x78, 0x50, 0x70, 0x59, 0x79,
  0xa6, 0x1e, 0xaa, 0x11, 0x19, 0xa3, 0x10, 0xab, 0x06, 0xb6, 0x08, 0xba, 0x00, 0xb3, 0x09, 0xbb,
  0xe6, 0xce, 0xea, 0xc2, 0xcb, 0xe3, 0xc3, 0xeb, 0xd6, 0xf6, 0xda, 0xfa, 0xd3, 0xf3, 0xdb, 0xfb,
  0x31, 0x8a, 0x3e, 0x86, 0x8f, 0x37, 0x87, 0x3f, 0x92, 0x21, 0x9e, 0x2e, 0x97, 0x27, 0x9f, 0x2f,
  0x61, 0x48, 0x6e, 0x46, 0x4f, 0x67, 0x47, 0x6f, 0x51, 0x71, 0x5e, 0x7e, 0x57, 0x77, 0x5f, 0x7f,
  0xa2, 0x18, 0xae, 0x16, 0x1f, 0xa7, 0x17, 0xaf, 0x01, 0xb2, 0x0e, 0xbe, 0x07, 0xb7, 0x0f, 0xbf,
  0xe2, 0xca, 0xee, 0xc6, 0xcf, 0xe7, 0xc7, 0xef, 0xd2, 0xf2, 0xde, 0xfe, 0xd7, 0xf7, 0xdf, 0xff
]

RC = [
  0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3E, 0x3D, 0x3B, 0x37, 0x2F, 0x1E, 0x3C, 0x39, 0x33,
  0x27, 0x0E, 0x1D, 0x3A, 0x35, 0x2B, 0x16, 0x2C, 0x18, 0x30, 0x21, 0x02, 0x05, 0x0B,
  0x17, 0x2E, 0x1C, 0x38, 0x31, 0x23, 0x06, 0x0D, 0x1B, 0x36, 0x2D, 0x1A, 0x34, 0x29,
  0x12, 0x24, 0x08, 0x11, 0x22, 0x04, 0x09, 0x13, 0x26, 0x0c, 0x19, 0x32, 0x25, 0x0a,
  0x15, 0x2a, 0x14, 0x28, 0x10, 0x20
]

Skinny64 = 64
Skinny128 = 128

Pt = [
  9, 15, 8, 13, 10, 14, 12, 11, 0, 1, 2, 3, 4, 5, 6, 7
]

M = [
  [1, 0, 1, 1],
  [1, 0, 0, 0],
  [0, 1, 1, 0],
  [1, 0, 1, 0]
]

NR = [
  #  TK1,TK2,TK3
  [32, 36, 40], # Skinny64
  [40, 48, 56], # Skinny128
]

X0 = 0b0000_0001
X1 = 0b0000_0010
X2 = 0b0000_0100
X3 = 0b0000_1000
X4 = 0b0001_0000
X5 = 0b0010_0000
X6 = 0b0100_0000
X7 = 0b1000_0000

def deep_clone(array)
  array.map do |el|
    el.is_a?(Array) ? deep_clone(el) : el
  end
end

class SkinnyConfig
  def initialize(options)
    @builder = CipherDagBuilder.new
    @version = options['version']
    unless @version == 64 || @version == 128
      raise "invalid cipher version. Should be either 64 or 128, given #{@version}"
    end
    @z = options['tweak-key']
    @nr = options['round'] || NR[@version / 64 - 1][@z - 1]

    if @version == 64
      @range = NIBBLE
      @s = @builder.declare_sbox_function([@range], [@range], SKINNY64_SBOX_LOOKUP_TABPLE)
      @lfsr = [
        nil,
        nil,
        @builder.declare_linear_function([@range], [@range], Specification::LFSR.new(poly: X3 ^ X2, direction: Specification::ShiftLeft, nb_bits: 4)),
        @builder.declare_linear_function([@range], [@range], Specification::LFSR.new(poly: X0 ^ X3, direction: Specification::ShiftRight, nb_bits: 4))
      ]
    else
      @range = BYTE
      @s = @builder.declare_sbox_function([@range], [@range], SKINNY128_SBOX_LOOKUP_TABPLE)
      @lfsr = [
        nil,
        nil,
        @builder.declare_linear_function([@range], [@range], Specification::LFSR.new(poly: X7 ^ X5, direction: Specification::ShiftLeft, nb_bits: 8)),
        @builder.declare_linear_function([@range], [@range], Specification::LFSR.new(poly: X0 ^ X6, direction: Specification::ShiftRight, nb_bits: 8))
      ]
    end

    @xor2 = @builder.declare_linear_function([@range, @range], [@range], Specification::BitwiseXOR.new)
    @xor3 = @builder.declare_linear_function([@range, @range, @range], [@range], Specification::BitwiseXOR.new)
    @xor4 = @builder.declare_linear_function([@range, @range, @range, @range], [@range], Specification::BitwiseXOR.new)
  end

  def sub_cells(internal_state)
    matrix(4, 4) { |i, j| @s.call(internal_state[i][j]) }
  end

  def add_constants(internal_state, r)
    rc = RC[r]
    c0 = rc & 0xF
    c1 = rc >> 4
    c2 = 0x02
    internal_state[0][0] = @xor2.call(internal_state[0][0], c0)
    internal_state[1][0] = @xor2.call(internal_state[1][0], c1)
    internal_state[2][0] = @xor2.call(internal_state[2][0], c2)
    internal_state
  end

  def add_round_tweakey(internal_state, tk)
    case @z
    when 1
      for i in 0..1
        for j in 0...4
          internal_state[i][j] = @xor2.call(internal_state[i][j], tk[1][i][j])
        end
      end
    when 2
      for i in 0..1
        for j in 0...4
          internal_state[i][j] = @xor3.call(internal_state[i][j], tk[1][i][j], tk[2][i][j])
        end
      end
    when 3
      for i in 0..1
        for j in 0...4
          internal_state[i][j] = @xor4.call(internal_state[i][j], tk[1][i][j], tk[2][i][j], tk[3][i][j])
        end
      end
    else
      raise 'invalid tk value'
    end
    internal_state
  end

  def shift_rows(internal_state)
    [
      internal_state[0],
      [internal_state[1][3], internal_state[1][0], internal_state[1][1], internal_state[1][2]],
      [internal_state[2][2], internal_state[2][3], internal_state[2][0], internal_state[2][1]],
      [internal_state[3][1], internal_state[3][2], internal_state[3][3], internal_state[3][0]]
    ]
  end

  def mix_columns(internal_state)
    mix_1 = Array.new(4) { |j| @xor2.call(internal_state[1][j], internal_state[2][j]) }
    mix_2 = Array.new(4) { |j| @xor2.call(internal_state[0][j], internal_state[2][j]) }
    mix_3 = Array.new(4) { |j| @xor2.call(internal_state[3][j], mix_2[j]) }
    [mix_3, internal_state[0], mix_1, mix_2]
  end

  def key_schedule(key)
    flattened_tk = key.each_slice(16).to_a
    # transform the rows into 4 x 4 matrices
    round_tweaky = flattened_tk.map do |row|
      row.each_slice(4).to_a
    end
    # Add fake tk[0]
    round_tweaky.unshift(nil)
    round_tweakeys = [deep_clone(round_tweaky)]
    for z in 1..@z
      @builder.named("TK#{z}[0]", round_tweaky[z])
    end
    for r in 1..@nr
      for z in 1..@z
        linear_tk = round_tweaky[z].flatten
        round_tweaky[z] = Array.new(linear_tk.size) { |idx| linear_tk[Pt[idx]] }.each_slice(4).to_a
      end
      for z in 2..@z
        for i in 0...2
          for j in 0...4
            round_tweaky[z][i][j] = @lfsr[z].call(round_tweaky[z][i][j])
          end
        end
      end
      for z in 1..@z
        @builder.named("TK#{z}[#{r}]", round_tweaky[z])
      end
      round_tweakeys.push(deep_clone(round_tweaky))
    end
    round_tweakeys
  end

  def encryption(plaintext, key)
    round_tweak_keys = key_schedule(key)
    state = plaintext
    for round_num in 0...@nr
      @builder.named("X_#{round_num}", state)
      state = sub_cells(state)
      @builder.named("SC_#{round_num}", state)
      state = add_constants(state, round_num)
      @builder.named("AC_#{round_num}", state)
      state = add_round_tweakey(state, round_tweak_keys[round_num])
      @builder.named("ART_#{round_num}", state)
      state = shift_rows(state)
      @builder.named("SR_#{round_num}", state)
      state = mix_columns(state)
      @builder.named("MC_#{round_num}", state)
    end
    state
  end

  def build
    plaintext = @builder.plaintext(4 * 4, @range).each_slice(4).to_a
    key = @builder.key(16 * @z, @range)
    @builder.ciphertext(encryption(plaintext, key))
    @builder.build
  end
end

def main
  options = process_options(ARGV)
  skinny = SkinnyConfig.new(options)
  puts skinny.build.to_json
end

main
