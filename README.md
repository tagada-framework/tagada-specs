# Tagada-specs

Tagada-Specs is a part of the [Tagada](https://gitlab.limos.fr/iia_lulibral/tagada) library. 

The aim of this library is to provide an high level API to describe ciphers.

The description of a new cipher can be done by using the `cipher_dag_builder` and some helper utilities.

## Requirements

- Ruby
- The `bundler` gem

See the Ruby [installation documentation](https://www.ruby-lang.org/en/documentation/installation/) for more details.

## Installation

Once `Ruby` and `bundler` are installed, the user can download the dependencies with:

```sh
bundle config set --local path vendor/bundle
bundle install
```

## Usage

The user can generate the Specification graph of the already implemented ciphers or implement a new cipher using the [Tagada-Structs](https://gitlab.limos.fr/iia_lorouque/tagada-structs) API. The [`cipher_dag_builder.rb`](builders/cipher_dag_builder.rb) provides helpers to make the creation process more user-friendly.

### Generation example

To generate the Specification graph for 3 rounds of WARP the user must use:

```sh
bundle exec ciphers/warp.rb -r 3
```

The command will print the graph in the standard output stream. The user can easily save the generated graph by redirecting the standard output to a file, *e.g.*:

```sh
bundle exec ciphers/warp.rb -r 3 > warp-3.spec.json
```

The parameters required depends on each cipher and are described in the [Already implemented ciphers](#already-implemented-ciphers) section.

If the user want to call the scripts from another directory, he should use the command:

```sh
BUNDLE_GEMFILE=<PATH to the Tadada-Specs/Gemfile> <PATH>/ciphers/warp.rb
```

## Already implemented ciphers

The already implemented ciphers are under the `ciphers` repository.

| Cipher   | Test Vectors | Parameters                                                   | Notes |
| -------- | ------------ | ------------------------------------------------------------ | ----- |
| Rijndael | Passed       | `-k`, `--keysize`: The key size in bits {128, 160, 192, 224, 256}. |       |
|          |              | `-p`, `--plainsize`: The plaintext size in bits {128, 160, 192, 224, 256}. |       |
|          |              | `-r`, `--rounds`: (optional) The number of rounds, the default value is the number of rounds given in the cipher specifications. |       |
| WARP     | Passed       | `-r`, `--rounds`: (optional) The number of rounds, the default value is the number of rounds given in the cipher specifications. |       |
| Skinny   | Passed       | `-v`, `--version`: The cipher version {64, 128}.             |       |
|          |              | `-tk`, `--tweak-key`: The tweak key configuration {0, 1, 2, 3}. The tweak key `0` excludes the keyschedule. |       |
|          |              | `-r`, `--rounds`: (optional) The number of rounds, the default value is the number of rounds given in the cipher specifications. |       |
| Twine    | Passed       | `-v`, `--version`: The cipher version {80, 128}.             |       |
|          |              | `-r`, `--rounds`: (optional) The number of rounds, the default value is the number of rounds given in the cipher specifications. |       |
| Midori   | Passed       | `-v`, `--version`: The cipher version {64, 128}.             |       |
|          |              | `-r`, `--rounds`: (optional) The number of rounds, the default value is the number of rounds given in the cipher specifications. |       |

